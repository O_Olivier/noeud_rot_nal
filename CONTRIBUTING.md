# MVP

Objectif : avoir les planètes et pouvoir lancer les Nyans quand on clique sur une planète

## Controleur

* Lanceur
	* Créer les planètes
	* Initialise la vue (thread)
	* Initialise le moteur (thread)
* Moteur
	* Listen pour les clics (vérifie s'il y a une planète cliquée)
	* Lance les Nyans et les fait bouger

## Modèle

* Monde
	* liste des planètes
* Planète
	* position

## Vue

