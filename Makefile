all : src/Lanceur.java
	javac -d bin/ src/*.java
	echo "Main-Class: patatedouce.nyan.Lanceur" > bin/MANIFEST.mf
	cd bin; jar -cvmf MANIFEST.mf Nyan.jar .; mv Nyan.jar ..
clean :
	rm *~
