# Concept

RTS sur le thème de Nyan cat

Plusieurs bases dans l'espace, chaque base envoie des Nyan cats

[Nyan](https://www.youtube.com/watch?v=SFoBxH0suLw)

## Ressources

* Chats : permet de créer des Nyan ou de travailler dans les bâtiments
* Tartine : permet de créer des Nyan
* Confiture : permet de créer des tartines
* Biscotte : permet de créer des tartines
* Blé : permet de créer des biscottes
* Fruit : permet de créer des confitures

## Bâtiments

* Tartinerie : fusionne de la confiture et des biscottes pour former des tartines
* Biscotterie : transforme le blé en biscottes
* Confiserie : transforme les fruits en confitures
* Ferme : produit du blé
* Fruiterie : produit des fruits
* Radiateur : abrite les chats

## Unités

* Nyan cats : Missile
* Nyan nyan cat : Nyan cat + arc-en-ciel (s'ils croisent une nébuleuse)
