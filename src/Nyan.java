package patatedouce.nyan;

/**
* @class Nyan
* @brief Représente une unité de base
* @author Catgolin
* @version 1
**/
class Nyan extends Positionnable implements Unite {
	private float vX, vY;
	/**
	* @param position
	* @param vitesse
	**/
	public Nyan(Point position, Point vitesse) {
		super(position);
		this.vX = vitesse.getX();
		this.vY = vitesse.getY();
	}
	@Override
	public Type type() {return Type.NYAN;}
	/**
	* @param x
	* @param y
	* @param z
	* @param vitesse
	**/
	public Nyan(float x, float y, float z, Point vitesse) {
		this(new Point(x, y, z), vitesse);
	}
	/**
	* @param x
	* @param y
	* @param vitesse
	**/
	public Nyan(float x, float y, Point vitesse) {
		this(x, y, 1, vitesse);
	}
	/**
	* @return vitesse
	**/
	public Point getVitesse() {
		return new Point(vX, vY);
	}
	/**
	* @param vX
	**/
	public void setVx(float vX) {
		this.vX = vX;
	}
	/**
	* @param vY
	**/
	public void setVy(float vY) {
		this.vY = vY;
	}
}
