package patatedouce.nyan;

import javax.swing.JPanel;
import javax.swing.JFrame;
import java.awt.Graphics;
import java.awt.Color;
import java.awt.event.MouseListener;
import java.awt.event.MouseEvent;

/**
 * @class Vue
 * @brief Assure la composition de l'affichage
 *
 * Rassemble les différents éléments de l'affichage
 * @author Catgolin
 * @version 1
 * @see Camera
 **/
public class Vue extends JFrame implements Runnable, MouseListener {
	private Moteur moteur;
	private Camera camera;
	/// long : durée minimale d'une frame en millisecondes
	private long min_frame = 1000/60;
	/// long : date de la dernière frame
	private long mise_a_jour;
	/// boolean : indique si l'affichage est achevé
	private boolean fini = true;
	/**
     * @brief Initialise tous les éléments d'affichage
     *
     * Initialise la fenêtre, la caméra
     * @param monde : le monde à représenter
	* @param moteur : le moteur de traitement des input
     * @since 1
     **/
	public Vue(Monde monde, Moteur moteur) {
		this.moteur = moteur;
		this.setTitle("Nyan");
		this.setSize(1000, 800);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.camera = new Camera(monde);
		this.setContentPane(new Panneau());
		this.setVisible(true);
		this.mise_a_jour = System.currentTimeMillis();
		this.addMouseListener(this);
	}
	/**
	* @brief Définit la durée minimale d'une frame
	* @param minFrame : la durée minimale d'une frame (en millisecondes)
	* @since 1
	**/
	public void setMinFrame(long minFrame) {
		this.min_frame = minFrame;
	}
	@Override
	public void run() {
		while(true) {
			this.mise_a_jour = System.currentTimeMillis();
			this.repaint();
			while(!this.fini) {
				try {
					Thread.sleep(1);
				} catch(InterruptedException e) {
					System.err.println("Thread.sleep Problem");
				}
			}
			long actu = System.currentTimeMillis();
			if(this.mise_a_jour - actu > this.min_frame) {
				try {
					Thread.sleep(this.min_frame - this.mise_a_jour + actu);
				} catch(InterruptedException e) {
					System.err.println("Thread.sleep Probleme");
				}
			}
		}
	}
	private class Panneau extends JPanel {
		@Override
		public void paintComponent(Graphics g) {
			Vue.this.fini = false;
			g.setColor(Color.BLACK);
			g.fillRect(0, 0, this.getWidth(), this.getHeight());
			Vue.this.camera.afficher(g);
			fini = true;
		}
	}
	@Override public void mouseClicked(MouseEvent e){
	    Positionnable p = camera.planeteCliquee(e.getX(), e.getY());
	    if(p != null && p instanceof Planete) {
		this.moteur.lancer((Planete)p);
	    }
	}
	@Override public void mouseEntered(MouseEvent e){}
	@Override public void mouseExited(MouseEvent e){}
	@Override public void mousePressed(MouseEvent e){}
	@Override public void mouseReleased(MouseEvent e){}
    }
