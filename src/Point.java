package patatedouce.nyan;

/**
* @class Point
* @brief Représente des coordonnées en 3 dimensions
* @author Catgolin
* @version 1
**/
public class Point {
	/// Les coordonnées
	private float x, y, z;
	/**
	* @param x :
	* @param y
	* @param z
	**/
	public Point(float x, float y, float z) {
		this.x = x; this.y = y; this.z = z;
	}
	/**
	* @param x
	* @param y
	**/
	public Point(float x, float y) { this(x,y,0);}
	/**
	* @param x
	**/
	public void setX(float x) {this.x = x;}
	/**
	* @param y
	**/
	public void setY(float y) {this.y = y;}
	/**
	* @param z
	**/
	public void setZ(float z) {this.z = z;}
	/**
	* @return x
	**/
	public float getX() {return this.x;}
	/**
	* @return y
	**/
	public float getY() {return this.y;}
	/**
	* @return z
	**/
	public float getZ() {return this.z;}
	/**
	* @return Le carré de la distance entre ce point et l'origine
	**/
	public float longueur() {return (float)Math.pow(this.x, 2)+(float)Math.pow(this.y, 2);}
}
