package patatedouce.nyan;

/**
 * @enum Type
 * @brief Types positionnables
 */
enum Type {
    PLANETE, NYAN
}

/**
   @class Positionnable
   @brief Caracterise un objet du monde
   *
   @author Catgolin
 */
abstract class Positionnable extends Point implements Comparable<Positionnable>{
	/// Nombre d'objets déjà créés
	private static int nombre;
	/// Identifiant de l'objet
	public final int id;
	/**
	* @param x
	* @param y
	* @param z
	**/
	public Positionnable(float x, float y, float z) {
		super(x, y, z);
		this.id = Positionnable.nombre;
		Positionnable.nombre++;
	}
	/**
	* @param x
	* @param y
	**/
	public Positionnable(float x, float y) {
		this(x, y, 0);
	}
	/**
	* @param point
	**/
	public Positionnable(Point point) {
		this(point.getX(), point.getY(), point.getZ());
	}
    
	@Override
	public int compareTo(Positionnable p){
		return Float.compare(getZ(), p.getZ());
	}

    /**
     * @return Le type du positionnable
     */
	public abstract Type type();
}
