package patatedouce.nyan;

import java.util.ArrayList;

/**
   @class Moteur
*/
public class Moteur implements Runnable{
    private Monde world;
    private ArrayList<Positionnable> listeMouvants;
    private long minFrame = 1000/60;

    /**
     * @param world
     */
    public Moteur(Monde world){
	this.world = world;
	this.listeMouvants = new ArrayList<Positionnable>();
    }

    @Override
    public void run() {
	while(true){
	    long debutFrame = System.currentTimeMillis();
	    for(int i = 0; i < this.listeMouvants.size(); i++) {
		Nyan n = (Nyan)this.listeMouvants.get(i);
		float x = n.getX();
		float y = n.getY();
		x += n.getVitesse().getX();
		y += n.getVitesse().getY();
		n.setX(x);
		n.setY(y);
	    }
	    if(System.currentTimeMillis() - debutFrame < this.minFrame) {
		try {
		    Thread.sleep(this.minFrame - System.currentTimeMillis() + debutFrame);
		} catch(InterruptedException e) {
		    //
		}
	    }
	}
    }

    /**
     * @param p
     * @brief Lance un Nyan cat
     */
    public void lancer(Planete p){
	Nyan n = new Nyan(p, new Point(1f,1f));
	world.ajouter(n);
	this.listeMouvants.add(n);
	System.out.println("Creation Nyan Cat");
    }
}
