package patatedouce.nyan;

public class Lanceur{
	public static void main(String[] args){
		Monde monde = new Monde();
		Moteur moteur = new Moteur(monde);
		Vue vue = new Vue(monde, moteur);
		for(int i = 0; i < 10; i++) {
			float x = (float)(Math.random()*vue.getWidth());
			float y = (float)(Math.random()*vue.getHeight());
			monde.ajouter(new Planete(x,y));
		}
		(new Thread(vue)).start();
		(new Thread(moteur)).start();
	}
}
