package patatedouce.nyan;

/**
   @class Planete
   @brief Planete
   @author O_Olivier
 */
class Planete extends Positionnable{

    /**
     * @param p
     */
    public Planete(Point p) {
	    super(p);
    }

    /**
     * @param x
     * @param y
     */
    public Planete(float x, float y) {
	    super(x, y, 0);
    }
    
    @Override
    public Type type() {return Type.PLANETE;}
}
