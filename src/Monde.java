package patatedouce.nyan;

import java.util.AbstractQueue;
import java.util.PriorityQueue;

/**
   @class Monde
   @brief Stocke les elements du monde
   @author O_Olivier
 */
class Monde{
    /// Liste des objets positionnables
    private PriorityQueue<Positionnable> listeObjets;

    /**
     * @brief Initialise la liste des objets (vide au départ)
     */
    public Monde(){
	listeObjets = new PriorityQueue<Positionnable>();
    }

    /**
     * @param p
     * @brief Ajoute un positionnable à la liste 
     */
    public void ajouter(Positionnable p) {
	    this.listeObjets.add(p);
    }

    /**
     * @return La liste des objets positionnables
     * sous forme de tableau
     */
    public Positionnable[] getListeObjets(){
	Positionnable[] tab = new Positionnable[0];
	return listeObjets.toArray(tab);
    }
}
