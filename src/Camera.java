package patatedouce.nyan;

import java.awt.Graphics;
import java.awt.Color;
import java.util.PriorityQueue;

/**
* @class Camera
* @brief Assure l'affichage du monde
*
* @author Catgolin
* @version 1
**/
public class Camera {
    /// Monde : l'ensemble des objets à représenter
    private Monde monde;
    /// int : taille du rayon d'une planète (en pixels)
    private int taillePlanete = 50;
    /// int : taille du rayon d'un nyan (en pixels)
    private int tailleNyan = 25;
    /**
     * @param monde : le monde à représenter
     **/
    public Camera(Monde monde) {
	this.monde = monde;
    }
    /**
     * @brief Affiche le monde
     * @param g : l'objet sur lequel représenter le monde
     **/
    public void afficher(Graphics g) {
		Positionnable[] objets = monde.getListeObjets();
		Positionnable o;
		for(int i = 0; i < objets.length; i++) {
			o = objets[i];
			int x = (int)o.getX();
			int y = (int)o.getY();
			switch(o.type()){
				case PLANETE:
					g.setColor(Color.RED);
					x -= taillePlanete;
					y -= taillePlanete;
					g.fillOval(x, y, 2*taillePlanete, 2*taillePlanete);
					break;
				case NYAN:
					g.setColor(Color.GREEN);
					x -= tailleNyan;
					y -= tailleNyan;
					g.fillOval(x, y, 2*tailleNyan, 2*tailleNyan);
					break;
			}
		}
	}
	/**
	* @brief Vérifie si les coordonnées données se superposent avec un objet ou non
	* @param p : l'objet à vérifier
	* @param x : coordonnée des abcisses (en pixels)
	* @param y : coordonnée des ordonnées (en pixels)
	* @return true si les coordonnées correspondent, false sinon
	* @author O_Olivier
	**/
    private boolean collision(Positionnable p, int x, int y){
	float dx = p.getX() - x;
	float dy = p.getY() - y;
	int taille = 0;
	switch(p.type()) {
		case PLANETE:
			taille = this.taillePlanete;
			break;
		case NYAN:
			taille = this.tailleNyan;
			break;
	}
	return dx*dx + dy*dy < (int)(Math.pow(taille, 2));
    }
    /**
    * @brief Retourne la planète qui se trouve aux coordonnées données
    * @param x : coordonnée des abcisses (en pixels)
    * @param y : coordonée des ordonnées (en pixels)
    * @return le positionnable peint à cet endroit s'il y en a un, ou null sinon
    * @author O_Olivier
    **/
    public Positionnable planeteCliquee(int x, int y){
		Positionnable[] objets = monde.getListeObjets();
		Positionnable o;
		for(int i = objets.length; i > -1; i--) {
			o = objets[i];
			if(collision(o, x, y))
				return o;
		}
		return null;
    }
}
