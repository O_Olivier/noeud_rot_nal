class Planete{
    private float x;
    private float y;
    
    public Planete(float x, float y){
	this.x = x;
	this.y = y;
    }

    public float getX(){
	return x;
    }
    public float getY(){
	return y;
    }    
}
